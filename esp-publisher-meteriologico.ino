#include <ESP8266WiFi.h>
#include <PubSubClient.h>

WiFiClient wifi_cliente;
PubSubClient cliente(wifi_cliente);

void setup() {
  // Iniciar serial port
  Serial.begin(115200);

  // Configurar wifi
  WiFi.begin("<REDE>", "<SENHA>");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  // Inicializar publisher MQTT
  cliente.setServer("<IP>", 1884);
  cliente.connect("MeuEsp");
}

void loop() {
 delay(5000);
 cliente.publish("pub_esp", "hello world");  
}
